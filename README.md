# keyword-driven

Keyword Driven Framework Automation

## techniques

- Selenium Webdriver: Browser Automation Tool
- Java: Program Language
- Apache Maven: Build Tool
- TestNG: Test Runner and Report Engine
- Page Object Model: Design Pattern
- Intellij: IDE
