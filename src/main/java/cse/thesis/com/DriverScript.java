package cse.thesis.com;

import cse.thesis.com.config.ActionKeywords;
import cse.thesis.com.config.Constants;
import cse.thesis.com.utility.ExcelUtil;
import cse.thesis.com.utility.LogUtil;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Properties;

public class DriverScript {

	private static ActionKeywords actionKeywords;
	private static String sActionKeyword;
	private static String sPageObject;
	private static Method method[];

	private static int iTestStep;
	private static int iTestLastStep;
	private static String sTestCaseID;
	private static String sRunMode;
	private static String sData;
	public static boolean bResult;
	public static Properties objectRepository;
	public static String pathDataEngine;

	public DriverScript(){
		actionKeywords = new ActionKeywords();
		method = actionKeywords.getClass().getMethods();
	}

	// arg[0] - arg[1] - arg[2] => pathExcelEngine - pathLog4j - pathObjectRepository
    public static void main(String[] args) throws Exception {
		pathDataEngine = args[0];
		ExcelUtil.setExcelFile(pathDataEngine);
		DOMConfigurator.configure(args[1]);

		File file = new File(args[2]);
		FileInputStream fs = new FileInputStream(file);
		objectRepository = new Properties(System.getProperties());
		objectRepository.load(fs);
		DriverScript startEngine = new DriverScript();
		startEngine.execute_TestCase();
    }
		
    private void execute_TestCase() throws Exception {
		int iTotalTestCases = ExcelUtil.getRowCount(Constants.Sheet_TestCases);
		for(int iTestcase = 1; iTestcase < iTotalTestCases; iTestcase++){
			bResult = true;
			sTestCaseID = ExcelUtil.getCellData(iTestcase, Constants.Col_TestCaseID, Constants.Sheet_TestCases);
			sRunMode = ExcelUtil.getCellData(iTestcase, Constants.Col_RunMode,Constants.Sheet_TestCases);
			if (sRunMode.equals("Yes")){
				LogUtil.startTestCase(sTestCaseID);
				iTestStep = ExcelUtil.getRowContains(sTestCaseID, Constants.Col_TestCaseID, Constants.Sheet_TestSteps);
				iTestLastStep = ExcelUtil.getTestStepsCount(Constants.Sheet_TestSteps, sTestCaseID, iTestStep);
				bResult=true;
				for (;iTestStep < iTestLastStep; iTestStep++){
					sActionKeyword = ExcelUtil.getCellData(iTestStep, Constants.Col_ActionKeyword,Constants.Sheet_TestSteps);
					sPageObject = ExcelUtil.getCellData(iTestStep, Constants.Col_PageObject, Constants.Sheet_TestSteps);
					sData = ExcelUtil.getCellData(iTestStep, Constants.Col_DataSet, Constants.Sheet_TestSteps);

					execute_Actions();
					if(bResult==false){
						ExcelUtil.setCellData(Constants.KEYWORD_FAIL,iTestcase,Constants.Col_Result,Constants.Sheet_TestCases);
						LogUtil.endTestCase(sTestCaseID);
						break;
					}
				}
				if(bResult==true){
					ExcelUtil.setCellData(Constants.KEYWORD_PASS,iTestcase,Constants.Col_Result,Constants.Sheet_TestCases);
					LogUtil.endTestCase(sTestCaseID);
				}
			}
		}
	}
     
	private static void execute_Actions() throws Exception {
		for (int i = 0; i < method.length; i++) {
			if (method[i].getName().equals(sActionKeyword)) {
				method[i].invoke(actionKeywords, sPageObject, sData);
				if (bResult == true) {
					ExcelUtil.setCellData(Constants.KEYWORD_PASS, iTestStep, Constants.Col_TestStepResult, Constants.Sheet_TestSteps);
					break;
				} else {
					ExcelUtil.setCellData(Constants.KEYWORD_FAIL, iTestStep, Constants.Col_TestStepResult, Constants.Sheet_TestSteps);
					ActionKeywords.closeBrowser("", "");
					break;
				}
			}
		}
	}
}