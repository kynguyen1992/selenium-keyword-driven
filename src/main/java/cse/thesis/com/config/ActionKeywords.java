package cse.thesis.com.config;

import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import cse.thesis.com.DriverScript;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import cse.thesis.com.utility.LogUtil;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static cse.thesis.com.DriverScript.objectRepository;
import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;
import static io.github.bonigarcia.wdm.DriverManagerType.FIREFOX;
import static io.github.bonigarcia.wdm.DriverManagerType.IEXPLORER;

public class ActionKeywords {
	
	public static WebDriver driver;

	// Alert Keywords
	public static void acceptAlert(String object, String data){
		try{
			LogUtil.info("Accept alert popup");
			driver.switchTo().alert().accept();
		}catch (Exception e){
			LogUtil.error(String.format("Not able to Accept alert popup --- {%s}", e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void dismissAlert(String object, String data){
		try{
			LogUtil.info("Dismiss alert popup");
			driver.switchTo().alert().dismiss();
		}catch (Exception e){
			LogUtil.error(String.format("Not able to Dismiss alert popup --- {%s}", e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void setAlertText(String object, String text){
		try{
			LogUtil.info(String.format("Set text {%s} in alert popup", text));
			driver.switchTo().alert().sendKeys(text);
		}catch (Exception e){
			LogUtil.error(String.format("Not able to Set text {%s} in alert popup --- {%s}", text, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyAlertMessage(String object, String expectedMessage){
		try{
			LogUtil.info(String.format("Verify text in alert popup equal {%s}", expectedMessage));
			String actualMessage = driver.switchTo().alert().getText();
			if (!actualMessage.equals(expectedMessage)){
				DriverScript.bResult = false;
			}
		}catch (Exception e){
			LogUtil.error(String.format("Not able to Set text {%s} in alert popup --- {%s}", expectedMessage, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	// Browser keyword
	public static void openBrowser(String object, String browserName){
		LogUtil.info(String.format("Opening {%s} Browser", browserName));
		try{
			if(browserName.equals("Firefox")){
				WebDriverManager.getInstance(FIREFOX).setup();
				driver = new FirefoxDriver();
				LogUtil.info("Firefox browser started");
			}
			else if(browserName.equals("IE")){
				WebDriverManager.getInstance(IEXPLORER).setup();
				driver = new InternetExplorerDriver();
				LogUtil.info("IE browser started");
			}
			else if(browserName.equals("Chrome")){
				WebDriverManager.getInstance(CHROME).setup();
				driver = new ChromeDriver();
				LogUtil.info("Chrome browser started");
			}

			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		}catch (Exception e){
			LogUtil.info(String.format("Not able to open the {%s} Browser --- {%s}", browserName, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

    public static void closeBrowser(String object, String data){
        try{
            LogUtil.info("Closing the browser");
            driver.quit();
        }catch(Exception e){
            LogUtil.error(String.format("Not able to Close the Browser --- {%s}", e.getMessage()));
            DriverScript.bResult = false;
        }
    }

    public static void backward(String object, String data){
        try{
            LogUtil.info("Backward to the previous page in browser");
            driver.navigate().back();
        }catch(Exception e){
            LogUtil.error(String.format("Not able to Backward to the previous page --- {%s}", e.getMessage()));
            DriverScript.bResult = false;
        }
    }

	public static void forward(String object, String data){
		try{
			LogUtil.info("Forward to the history page in browser");
			driver.navigate().forward();
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Forward to the history page --- {%s}", e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void refresh(String object, String data){
		try{
			LogUtil.info(String.format("Refresh the current page"));
			driver.navigate().refresh();
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Refresh the current page --- {%s}", data, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void deleteAllCookies(String object, String data){
		try{
			LogUtil.info("Delete all Cookies");
			driver.manage().deleteAllCookies();
		}catch(Exception e){
			LogUtil.info(String.format("Not able to delete all cookies --- {%s}", e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void executeJavaScript(String object, String javaScript){
		try{
			LogUtil.info(String.format("Execute java script {%s} to {%s} element", javaScript, object));
			if (object.equals("")){
				((JavascriptExecutor) driver).executeScript(javaScript);
			}else{
				((JavascriptExecutor) driver).executeScript(javaScript, driver.findElement(By.xpath(objectRepository.getProperty(object))));
			}
		}catch(Exception e){
			LogUtil.info(String.format("Not able to execute java script {%s} to {%s} element --- {%s}", javaScript, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void navigateToUrl(String object, String url){
		try{
			LogUtil.info(String.format("Navigating to URL {%s}", url));
			driver.get(url);
		}catch(Exception e){
			LogUtil.info(String.format("Not able to navigate {%s} --- {%s}", url, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyWindowTitle(String object, String expectedTitle){
		try{
			LogUtil.info(String.format("Verify window title {%s}", expectedTitle));
			String actualTitle = driver.getTitle();
			if (!actualTitle.equals(expectedTitle)){
				DriverScript.bResult = false;
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Verify window title {%s} --- {%s}", expectedTitle, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void captureScreenshot(String object, String fileName){
		try{
			LogUtil.info(String.format("Capture screenshot with file name {%s}", fileName));
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(String.format(Constants.Path_Image), fileName));
		}catch(Exception e){
			LogUtil.info(String.format("Not able to Capture screenshot with file name {%s} --- {%s}", fileName, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void threadSleep(String object, String time){
		try{
			LogUtil.info(String.format("Thread sleep in {%s} seconds", time));
			Thread.sleep(Integer.parseInt(time) * 1000);
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Thread sleep in {%s} seconds --- {%s}", time, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	// Keyboard Keywords
	public static void pressKeyboards(String object, String data){
		try{
			LogUtil.info(String.format("Press {%s} on {%s} element", data, object));
			WebElement element = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			if ("Arrow_Down".equals(data)) {
				element.sendKeys(Keys.ARROW_DOWN);
			} else if ("Arrow_Up".equals(data)) {
				element.sendKeys(Keys.ARROW_UP);
			} else if ("Arrow_Left".equals(data)) {
				element.sendKeys(Keys.ARROW_LEFT);
			} else if ("Arrow_Right".equals(data)) {
				element.sendKeys(Keys.ARROW_RIGHT);
			} else if ("Back_Space".equals(data)) {
				element.sendKeys(Keys.BACK_SPACE);
			} else if ("Shift".equals(data)) {
				element.sendKeys(Keys.SHIFT);
			} else if ("Enter".equals(data)) {
				element.sendKeys(Keys.ENTER);
			} else if ("Space".equals(data)) {
				element.sendKeys(Keys.SPACE);
			} else if ("Tab".equals(data)) {
				element.sendKeys(Keys.TAB);
			} else if ("Escape".equals(data)) {
				element.sendKeys(Keys.ESCAPE);
			} else if ("Insert".equals(data)) {
				element.sendKeys(Keys.INSERT);
			} else if ("F1".equals(data)) {
				element.sendKeys(Keys.F1);
			} else if ("F2".equals(data)) {
				element.sendKeys(Keys.F2);
			} else if ("F3".equals(data)) {
				element.sendKeys(Keys.F3);
			} else if ("F4".equals(data)) {
				element.sendKeys(Keys.F4);
			} else if ("F5".equals(data)) {
				element.sendKeys(Keys.F5);
			} else if ("F6".equals(data)) {
				element.sendKeys(Keys.F6);
			} else if ("F7".equals(data)) {
				element.sendKeys(Keys.F7);
			} else if ("F8".equals(data)) {
				element.sendKeys(Keys.F8);
			} else if ("F9".equals(data)) {
				element.sendKeys(Keys.F9);
			} else if ("F10".equals(data)) {
				element.sendKeys(Keys.F10);
			} else if ("F11".equals(data)) {
				element.sendKeys(Keys.F11);
			} else if ("F12".equals(data)) {
				element.sendKeys(Keys.F12);
			} else {
				LogUtil.error(String.format("Invalid key {%s}", data));
				DriverScript.bResult = false;
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Press {%s} on {%s} element --- {%s}", data, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void input(String object, String text){
		try{
			LogUtil.info(String.format("Entering text {%s} in {%s} element", text, object));
			driver.findElement(By.xpath(objectRepository.getProperty(object))).sendKeys(text);
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Enter text {%s} in {%s} element ---{%s}", text, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	// Mouse Keyword
	public static void click(String object, String data){
		try{
			LogUtil.info(String.format("Clicking on {%s} element ", object));
			driver.findElement(By.xpath(objectRepository.getProperty(object))).click();
		}catch(Exception e){
			LogUtil.error(String.format("Not able to click {%s} element --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void mouseOver(String object, String data){
		try{
			LogUtil.info(String.format("Mouse hover on {%s} element ", object));
			Actions action = new Actions(driver);
			WebElement element = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			action.moveToElement(element).build().perform();
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Mouse hover on {%s} element --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void rightClick(String object, String data){
		try{
			LogUtil.info(String.format("Right click on {%s} element ", object));
			Actions action = new Actions(driver);
			WebElement element = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			action.contextClick(element).build().perform();
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Right click on {%s} element --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void dragAndDrop(String listObject, String data){
		String sourceName = "";
		String destinationName = "";
		try{
			String[] objects = listObject.split("-");
			sourceName = objects[0];
			destinationName = objects[1];

			LogUtil.info(String.format("Drag and drop {%s} source element to {%s} destination element", sourceName, destinationName));
			WebElement sourceElement = driver.findElement(By.xpath(objectRepository.getProperty(sourceName)));
			WebElement destinationElement = driver.findElement(By.xpath(objectRepository.getProperty(destinationName)));
			Actions builder = new Actions(driver);
			builder.dragAndDrop(sourceElement, destinationElement).build().perform();
		}catch(Exception e){
			LogUtil.error(String.format("Not able to drag and drop {%s} source element to {%s} destination element --- {%s}", sourceName, destinationName, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void scrollElementIntoView(String object, String data){
		try{
			LogUtil.info(String.format("Scroll {%s} element into view", object));
			WebElement element = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Right click on {%s} element --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	// Web Element Keyword
	public static void clickOffset(String object, String xyOffset){
		try{
			LogUtil.info(String.format("Clicking {%s} element at offset x-y {%s}", object, xyOffset));
			WebElement toElement = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			String[] offset = xyOffset.split("-");
			int xOffset = Integer.parseInt(offset[0]);
			int yOffset = Integer.parseInt(offset[1]);
			Actions builder = new Actions(driver);
			builder.moveToElement(toElement, xOffset, yOffset).click().build().perform();
		}catch(Exception e){
			LogUtil.error(String.format("Not able to click {%s} element at offset x-y {%s} --- {%s}", object, xyOffset, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void dragAndDropByOffset(String object, String xyOffset){
		try{
			LogUtil.info(String.format("Drag and drop {%s} element to offset x-y {%s}", object, xyOffset));
			WebElement toElement = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			String[] offset = xyOffset.split("-");
			int xOffset = Integer.parseInt(offset[0]);
			int yOffset = Integer.parseInt(offset[1]);
			Actions builder = new Actions(driver);
			builder.dragAndDropBy(toElement, xOffset, yOffset).build().perform();
		}catch(Exception e){
			LogUtil.error(String.format("Not able to drag and drop {%s} element to offset x-y {%s} --- {%s}", object, xyOffset, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyElementAttribute(String object, String attributeAndValue){
		String[] str = attributeAndValue.split("-");
		String attribute = str[0];
		String expectedValue = str[1];
		try{
			LogUtil.info(String.format("Verify attribute {%s} of {%s} element is {%s}", attribute, object, expectedValue));
			WebElement element = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			String actualValue = element.getAttribute(attribute);
			if (!actualValue.equals(expectedValue)){
				DriverScript.bResult = false;
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Verify attribute {%s} of {%s} element is {%s} --- {%s}", attribute, object, expectedValue, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyElementText(String object, String expectedText){
		try{
			LogUtil.info(String.format("Verify text {%s} of {%s} element", expectedText, object));
			String actualText = driver.findElement(By.xpath(objectRepository.getProperty(object))).getText();
			if (!actualText.equals(expectedText)){
                LogUtil.info(String.format("Actual text {%s} is not equal expected text {%s}", actualText, expectedText));
				DriverScript.bResult = false;
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Verify text {%s} of {%s} element --- {%s}", expectedText, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyElementPresent(String object, String data){
		try{
			LogUtil.info(String.format("Verify {%s} element is present", object));
			if (driver.findElements(By.xpath(objectRepository.getProperty(object))).size() == 0){
				DriverScript.bResult = false;
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Verify {%s} element is present --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyElementVisible(String object, String data){
		try{
			LogUtil.info(String.format("Verify {%s} element is visible", object));
			if (!driver.findElement(By.xpath(objectRepository.getProperty(object))).isDisplayed()){
				DriverScript.bResult = false;
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Verify {%s} element is visible --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyElementEnable(String object, String data){
		try{
			LogUtil.info(String.format("Verify {%s} element is enabled", object));
			if (!driver.findElement(By.xpath(objectRepository.getProperty(object))).isEnabled()){
				DriverScript.bResult = false;
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Verify {%s} element is enabled --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void waitElementClickable(String object, String data){
		try{
			LogUtil.info(String.format("Wait {%s} element is clickable in {%s} second", object, data));
			WebElement element = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			WebDriverWait wait = new WebDriverWait(driver, Integer.parseInt(data));
			wait.until(ExpectedConditions.elementToBeClickable(element));
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Wait {%s} element is clickable in {%s} second --- {%s}", object, data, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void waitElementPresent(String object, String data){
		try{
			LogUtil.info(String.format("Wait {%s} element is present", object));
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(objectRepository.getProperty(object))));
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Wait {%s} element is present--- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void waitElementVisible(String object, String data){
		try{
			LogUtil.info(String.format("Wait {%s} element is visible", object));
			WebElement element = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.visibilityOf(element));
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Wait {%s} element is visible--- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	// Radio and Checkbox Keywords
	public static void check(String object, String data){
		try{
			LogUtil.info(String.format("Check {%s} element ", object));
			WebElement checkbox = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			if (!checkbox.isSelected()){
				checkbox.click();
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Check {%s} element --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void uncheck(String object, String data){
		try{
			LogUtil.info(String.format("Uncheck {%s} element ", object));
			WebElement checkbox = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			if (checkbox.isSelected()){
				checkbox.click();
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Uncheck {%s} element --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyElementChecked(String object, String data){
		try{
			LogUtil.info(String.format("Verify element {%s} checked", object));
			WebElement checkbox = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			if (!checkbox.isSelected()){
				DriverScript.bResult = false;
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Verify element {%s} checked --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyElementNotChecked(String object, String data){
		try{
			LogUtil.info(String.format("Verify element {%s} not checked", object));
			WebElement checkbox = driver.findElement(By.xpath(objectRepository.getProperty(object)));
			if (checkbox.isSelected()){
				DriverScript.bResult = false;
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Verify element {%s} not checked --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	// Combobox and Drop-down list Keywords
	public static void deselectAllOption(String object, String data){
		try{
			LogUtil.info(String.format("Deselect all option for combobox {%s} ", object));
			Select listbox = new Select(driver.findElement(By.xpath(objectRepository.getProperty(object))));
			listbox.deselectAll();
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Deselect all option for combobox {%s} --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void deselectOptionByIndex(String object, String range){
		try{
			LogUtil.info(String.format("Deselect option by index {%s} for combobox {%s} ", range, object));
			Select listbox = new Select(driver.findElement(By.xpath(objectRepository.getProperty(object))));
			// handle index
			if (range.contains(",")){
				//"2,3" - index 2 and 3
				String[] indexes = range.split(",");
				for (String index : indexes){
					listbox.deselectByIndex(Integer.parseInt(index));
				}
			}else if (range.contains("-")){
				//"2-5" - index 2 to 5 (2,3,4,5)
				String[] indexes = range.split("-");
				int startIndex = Integer.parseInt(indexes[0]);
				int endIndex = Integer.parseInt(indexes[1]);
				for (int i = startIndex; i <= endIndex; i++){
					listbox.deselectByIndex(i);
				}
			}else{
				// 2 - index 2
				listbox.deselectByIndex(Integer.parseInt(range));
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Deselect option by index {%s} for combobox {%s} --- {%s}", range, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void deselectOptionByValue(String object, String value){
		try{
			LogUtil.info(String.format("Deselect option by value {%s} for combobox {%s} ", value, object));
			Select listbox = new Select(driver.findElement(By.xpath(objectRepository.getProperty(object))));
			listbox.deselectByValue(value);
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Deselect option by value {%s} for combobox {%s} --- {%s}", value, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void deselectOptionByVisibleText(String object, String text){
		try{
			LogUtil.info(String.format("Deselect option by visible text {%s} for combobox {%s} ", text, object));
			Select listbox = new Select(driver.findElement(By.xpath(objectRepository.getProperty(object))));
			listbox.deselectByVisibleText(text);
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Deselect option by visible text {%s} for combobox {%s} --- {%s}", text, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyNumberOfSelectedOption(String object, String expectedNumber){
		try{
			LogUtil.info(String.format("Verify number of selected option is {%s} for combobox {%s} ", expectedNumber, object));
			Select listbox = new Select(driver.findElement(By.xpath(objectRepository.getProperty(object))));
			List<WebElement> options = listbox.getAllSelectedOptions();
			int actualNumber = options.size();
			if (actualNumber != Integer.parseInt(expectedNumber)){
				DriverScript.bResult = false;
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Verify number of selected option is {%s} for combobox {%s} --- {%s}", expectedNumber, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyNumberOfTotalOption(String object, String expectedNumber){
		try{
			LogUtil.info(String.format("Verify number of total option is {%s} for combobox {%s} ", expectedNumber, object));
			Select listbox = new Select(driver.findElement(By.xpath(objectRepository.getProperty(object))));
			List<WebElement> options = listbox.getOptions();
			int actualNumber = options.size();
			if (actualNumber != Integer.parseInt(expectedNumber)){
				DriverScript.bResult = false;
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Verify number of total option is {%s} for combobox {%s} --- {%s}", expectedNumber, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void selectOptionByIndex(String object, String index){
		try{
			LogUtil.info(String.format("Select option by index {%s} for combobox {%s} ", index, object));
			Select listbox = new Select(driver.findElement(By.xpath(objectRepository.getProperty(object))));
			listbox.selectByIndex(Integer.parseInt(index));
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Select option by index {%s} for combobox {%s} --- {%s}", index, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void selectOptionByValue(String object, String value){
		try{
			LogUtil.info(String.format("Select option by value {%s} for combobox {%s} ", value, object));
			Select listbox = new Select(driver.findElement(By.xpath(objectRepository.getProperty(object))));
			listbox.selectByValue(value);
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Select option by value {%s} for combobox {%s} --- {%s}", value, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void selectOptionByVisibleText(String object, String text){
		try{
			LogUtil.info(String.format("Select option by visible text {%s} for combobox {%s} ", text, object));
			Select listbox = new Select(driver.findElement(By.xpath(objectRepository.getProperty(object))));
			listbox.selectByVisibleText(text);
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Select option by visible text {%s} for combobox {%s} --- {%s}", text, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyOptionNotPresentByValue(String object, String value){
		try{
			LogUtil.info(String.format("Verify option is not present by value {%s} for combobox {%s} ", value, object));
			Select listbox = new Select(driver.findElement(By.xpath(objectRepository.getProperty(object))));
			List<WebElement> options = listbox.getOptions();
			for (WebElement option : options){
				String actualValue = option.getText();
				if (actualValue.equals(value)){
					DriverScript.bResult = false;
					break;
				}
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Verify option is not present by value {%s} for combobox {%s} --- {%s}", value, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void verifyOptionPresentByValue(String object, String value){
		try{
			LogUtil.info(String.format("Verify option is present by value {%s} for combobox {%s} ", value, object));
			Select listbox = new Select(driver.findElement(By.xpath(objectRepository.getProperty(object))));
			List<WebElement> options = listbox.getOptions();
			boolean found = false;
			for (WebElement option : options){
				String actualValue = option.getText();
				if (actualValue.equals(value)){
					found = true;
					break;
				}
			}

			if (!found){
				DriverScript.bResult = false;
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Verify option is present by value {%s} for combobox {%s} --- {%s}", value, object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	// Frame Keywords
	public static void switchToDefaultFrame(String object, String data){
		try{
			LogUtil.info(String.format("Switch to default frame"));
			driver.switchTo().defaultContent();
		}catch (Exception e){
			LogUtil.error(String.format("Not able to Switch to default frame --- {%s}", e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void switchToFrame(String object, String data){
		try{
			LogUtil.info(String.format("Switch to {%s} frame", object));
			driver.switchTo().frame(driver.findElement(By.xpath(objectRepository.getProperty(object))));
		}catch (Exception e){
			LogUtil.error(String.format("Not able to Switch to {%s} frame --- {%s}", object, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	// Window Keywords
	public static void closeWindowIndex(String object, String index){
		try{
			LogUtil.info(String.format("Close window at index {%s}", index));
			String[] allWindows = (String[]) driver.getWindowHandles().toArray();
			String mainwindow = driver.getWindowHandle();
			driver.switchTo().window(allWindows[Integer.parseInt(index)]).close();
			driver.switchTo().window(mainwindow);
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Close window at index {%s} --- {%s}", index, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void switchToWindowIndex(String object, String index){
		try{
			LogUtil.info(String.format("Switch to window at index {%s}", index));
			String[] allWindows = (String[]) driver.getWindowHandles().toArray();
			driver.switchTo().window(allWindows[Integer.parseInt(index)]);
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Switch to window at index {%s} --- {%s}", index, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void closeWindowTitle(String object, String expectedTitle){
		try{
			LogUtil.info(String.format("Close window with title {%s}", expectedTitle));
			Set <String> windows = driver.getWindowHandles();
			String mainwindow = driver.getWindowHandle();
			for (String handle: windows){
				driver.switchTo().window(handle);
				String pageTitle = driver.getTitle();
				if(pageTitle.equals(expectedTitle)){
					driver.close();
					break;
				}
			}

			driver.switchTo().window(mainwindow);
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Close window with title {%s} --- {%s}", expectedTitle, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void switchToWindowTitle(String object, String expectedTitle){
		try{
			LogUtil.info(String.format("Switch to window with title {%s}", expectedTitle));
			Set <String> windows = driver.getWindowHandles();
			for (String handle: windows){
				driver.switchTo().window(handle);
				String pageTitle = driver.getTitle();
				if(pageTitle.equals(expectedTitle)){
					break;
				}
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Switch to window with title {%s} --- {%s}", expectedTitle, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void closeWindowUrl(String object, String expectedUrl){
		try{
			LogUtil.info(String.format("Close window contains Url {%s}", expectedUrl));
			Set <String> windows = driver.getWindowHandles();
			String mainwindow = driver.getWindowHandle();
			for (String handle: windows){
				driver.switchTo().window(handle);
				String actualUrl = driver.getCurrentUrl();
				if(actualUrl.equals(expectedUrl)){
					driver.close();
					break;
				}
			}

			driver.switchTo().window(mainwindow);
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Close window contains Url {%s} --- {%s}", expectedUrl, e.getMessage()));
			DriverScript.bResult = false;
		}
	}

	public static void switchToWindowUrl(String object, String expectedUrl){
		try{
			LogUtil.info(String.format("Switch to window contains Url {%s}", expectedUrl));
			Set <String> windows = driver.getWindowHandles();
			for (String handle: windows){
				driver.switchTo().window(handle);
				String actualUrl = driver.getCurrentUrl();
				if(actualUrl.equals(expectedUrl)){
					break;
				}
			}
		}catch(Exception e){
			LogUtil.error(String.format("Not able to Switch to window contains Url {%s} --- {%s}", expectedUrl, e.getMessage()));
			DriverScript.bResult = false;
		}
	}
}