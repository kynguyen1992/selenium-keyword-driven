package cse.thesis.com.utility;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import cse.thesis.com.DriverScript;
import cse.thesis.com.config.Constants;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {
    private static XSSFSheet ExcelWSheet;
    private static XSSFWorkbook ExcelWBook;
    private static org.apache.poi.ss.usermodel.Cell Cell;
    private static XSSFRow Row;

    public static void setExcelFile(String Path) {
        try {
            FileInputStream ExcelFile = new FileInputStream(Path);
            ExcelWBook = new XSSFWorkbook(ExcelFile);
        } catch (Exception e){
            LogUtil.error("Class Utils | Method setExcelFile | Exception desc : "+e.getMessage());
            DriverScript.bResult = false;
        }
    }

    public static String getCellData(int RowNum, int ColNum, String SheetName ){
        try{
            ExcelWSheet = ExcelWBook.getSheet(SheetName);
            Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
            String CellData = Cell.getStringCellValue();
            return CellData;
         }catch (Exception e){
            LogUtil.error("Class Utils | Method getCellData | Exception desc : "+e.getMessage());
             DriverScript.bResult = false;
             return "";
         }
     }

    public static int getRowCount(String SheetName){
        int iNumber=0;
        try {
            ExcelWSheet = ExcelWBook.getSheet(SheetName);
            iNumber=ExcelWSheet.getLastRowNum()+1;
        } catch (Exception e){
            LogUtil.error("Class Utils | Method getRowCount | Exception desc : "+e.getMessage());
            DriverScript.bResult = false;
        }
        return iNumber;
    }

    public static int getRowContains(String sTestCaseName, int colNum,String SheetName){
        int iRowNum=0;
        try {
            int rowCount = ExcelUtil.getRowCount(SheetName);
            for (; iRowNum<rowCount; iRowNum++){
                if  (ExcelUtil.getCellData(iRowNum,colNum,SheetName).equalsIgnoreCase(sTestCaseName)){
                    break;
                }
            }
        } catch (Exception e){
            LogUtil.error("Class Utils | Method getRowContains | Exception desc : "+e.getMessage());
            DriverScript.bResult = false;
            }
        return iRowNum;
    }

    public static int getTestStepsCount(String SheetName, String sTestCaseID, int iTestCaseStart){
        try {
            for(int i = iTestCaseStart; i< ExcelUtil.getRowCount(SheetName); i++){
                if(!sTestCaseID.equals(ExcelUtil.getCellData(i, Constants.Col_TestCaseID, SheetName))){
                    int number = i;
                    return number;
                    }
                }
            ExcelWSheet = ExcelWBook.getSheet(SheetName);
            int number=ExcelWSheet.getLastRowNum()+1;
            return number;
        } catch (Exception e){
            LogUtil.error("Class Utils | Method getRowContains | Exception desc : "+e.getMessage());
            DriverScript.bResult = false;
            return 0;
        }
    }

    @SuppressWarnings("static-access")
    public static void setCellData(String Result,  int RowNum, int ColNum, String SheetName){
           try{

               ExcelWSheet = ExcelWBook.getSheet(SheetName);
               Row  = ExcelWSheet.getRow(RowNum);
               Cell = Row.getCell(ColNum, org.apache.poi.ss.usermodel.Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
               if (Cell == null) {
                   Cell = Row.createCell(ColNum);
                   Cell.setCellValue(Result);
                } else {
                    Cell.setCellValue(Result);
                }
                 FileOutputStream fileOut = new FileOutputStream(DriverScript.pathDataEngine);
                 ExcelWBook.write(fileOut);
                 fileOut.close();
                 ExcelWBook = new XSSFWorkbook(new FileInputStream(DriverScript.pathDataEngine));
             }catch(Exception e){
                 DriverScript.bResult = false;
             }
        }
}